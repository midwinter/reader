    FROM node:alpine
    WORKDIR /usr/reader
    COPY package.json .
    COPY yarn.lock .
    COPY packages/server/package.json ./packages/server/
    # COPY packages/server/yarn.lock ./packages/server/
    COPY packages/web/package.json ./packages/web/
    # COPY packages/web/yarn.lock ./packages/web/
    RUN yarn install
    COPY . . 
    CMD ["yarn",  "start"]