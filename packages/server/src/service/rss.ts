import Parser from "rss-parser";
const parser = new Parser();

enum Category {
  Programming = 1,
  Sports,
  News
}
interface Feed {
  url: string;
  category: Category;
}
const sources: Feed[] = [
  { url: "https://stoeten.substack.com/feed", category: Category.Sports }
];

export async function loadFeeds() {
  sources.forEach(async source => {
    const feed = await parser.parseURL(source.url);
    console.log(feed);
  });
}
