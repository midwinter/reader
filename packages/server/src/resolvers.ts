import { loadFeeds } from "./service/rss";

export default {
  Query: {
    async hello(obj: any, { subject }: { subject: string }) {
      await loadFeeds()
      return `Hello, ${subject}! from Server`;
    }
  }
};
